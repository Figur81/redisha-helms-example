# Redis helm example
### This repository is a introduction to helm usage.

[comment]: <> (Insert a diagram here to show a fast idea of what's helm.)
# Topics checklist
If you not know:
* [Redis](https://redis.io/topics/introduction)
* [Helms](https://helm.sh)

you need understand a basic of:
* [kubernetes](https://kubernetes.io/docs/concepts/overview/)
* [network ports usage](https://en.wikipedia.org/wiki/Port_(computer_networking))

# Hands-on!
[![asciicast](https://asciinema.org/a/ZliOdut3pYrgjI6RRKUBmKLgS.svg)](https://asciinema.org/a/ZliOdut3pYrgjI6RRKUBmKLgS)

## Let's see these terminal codes
## Show pods and services behavior
```console
watch microk8s kubectl get pods --all-namespaces
watch microk8s kubectl get services --all-namespaces
```

## Install redis stack
```console 
helm install redis bitnami/redis --values values.yaml
```

## Check redis health
```console
microk8s kubectl port-forward svc/redis-redis-ha 6379:6379
redis-cli -h localhost -p 6379 -a pass ping
```
